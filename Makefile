CC = gcc

tester_file: secrab_parser_lib.a 
	$(CC) -o tester_file secrab_tester_main.c secrab_parser_lib.a libllhttp.a

secrab_parser_lib.a: secrab_parser.o
	ar rcs secrab_parser_lib.a secrab_parser.o

secrab_parser.o: secrab_parser.h secrab_tester_main.c secrab_parser.c
	$(CC) -c secrab_parser.c secrab_tester_main.c 
