#include <stdint.h>
#include <stddef.h>
#include "constants.h"

// typedef int (*llhttp_data_cb)(llhttp_t*, const char *at, size_t length);


struct L7ParserContextType
{
    void *pUserData;
    void *pCallback;

    int nType;
    char * requestUrl;

    void *m_pContext;
    union 
    {
        struct L7ParserHTTPJobType
        {
            char HeaderBuffer[8192];
            int test_int;

            int32_t _index;
            void* _span_pos0;
            void* _span_cb0;
            int32_t error;
            const char* reason;
            const char* error_pos;
            void* data;
            void* _current;
            uint64_t content_length;
            uint8_t type;
            uint8_t method;
            uint8_t http_major;
            uint8_t http_minor;
            uint8_t header_state;
            // uint8_t lenient_flags;
            // uint8_t upgrade;
            // uint8_t finish;
            uint16_t flags;
            uint16_t status_code;
            uint8_t initial_message_completed;
            void* settings;
            //....
        } l7_jType;

        struct L7ParserRDPJobType
        {
            //....
            int jType;
        } l7_rdpjType;
    } l7_union;
} ;

typedef int (*l7parser_cb)(struct L7ParserHTTPJobType*);
typedef int (*l7parser_data_cb)(struct L7ParserHTTPJobType*, const char * at, size_t length);

struct L7ParserCallbackInterface
{
    l7parser_cb on_message_begin;
    l7parser_data_cb on_url;
    l7parser_data_cb on_status;
    l7parser_data_cb on_method;
    l7parser_data_cb on_version;
    l7parser_data_cb on_header_field;
    l7parser_data_cb on_header_value;
    l7parser_data_cb on_chunk_extension_name;
    l7parser_data_cb on_chunk_extension_value;
    l7parser_cb on_headers_complete;
    l7parser_data_cb on_body;
    l7parser_data_cb on_message_complete;
    l7parser_data_cb on_url_complete;
    l7parser_data_cb on_status_complete;
    l7parser_data_cb on_method_complete;
    l7parser_data_cb on_version_complete;
    l7parser_data_cb on_header_field_complete;
    l7parser_data_cb on_header_value_complete;
    l7parser_data_cb on_chunk_extension_name_complete;
    l7parser_data_cb on_chunk_extension_value_complete;
    l7parser_cb on_chunk_header;
    l7parser_cb on_chunk_complete;
    l7parser_cb on_reset;
};

// * void * L7Parser_Context_Create(int nType, void *pCallback, void *pUserData)
// - nType: L7P_HTTP_REQ, L7P_HTTP_RES, ...
// - pCallback : Structure pointer of callback functions according to Type
// - pUserData : The caller's cookie data pointer
// - Return : Context pointer (structure, first member must be UserData pointer)
void * L7Parser_Context_Create(int nType, void *pCallback, void *pUserData);


// * int L7Parser_Context_Destroy(void *pContext)
// - pContext : Destroy skill Context pointer
// - Return : Success / Failure
int L7Parser_Context_Destroy(void *pContext);


// 2. * Контекст эхлүүлэх, орчны тохиргоо (Шаардлагатай юу? Үүнийг үүсгэх функцэд нэгтгэж болох уу?) 
// int L7Parser_Context_Init(void *pContext, ...)
// - Return : Success / Failure
int L7Parser_Context_Init(void *pContext);


// - pData: data to be analyzed (continuous partial data)
// - nDataSize : data size
// - Return : Success / Failure

// Q&A: (continuous partial data): is it streaming data from secrab's server? 
// 
// We are writing a simple webserver for *pData to test the parser, 
// 
int L7Parser_Parse(void *pContext, void *pData, size_t nDataSize);
