# secrab-http-parser
## 

[![IT wizard](http://www.itwizard.mn/img/logo_blck.89ed88a1.png)](http://www.itwizard.mn/)

secrab-http-parser is a software linker created using C/C++ programming language.

- Can parse REQUEST (L7P_HTTP_REQ)
- Can parse RESPONSE (L7P_HTTP_RES)
- ✨M***********************g

## Features

- [L7Parser_Context_Create] (requires more information)
- [L7Parser_Context_Destroy] (requires more information)
- [L7Parser_Context_Init] (prepare initialazation enviroment)
- [L7Parser_Parse] (requires more information)

[L7Parser_Context_Create] have three parameters and return pointer. (int nType, void *pCallback, char *pUserData)
    nType: L7P_HTTP_REQ, L7P_HTTP_RES, ...
    pCallback : Structure pointer of callback functions according to Type
    pUserData : The caller's cookie data pointer
Return : Context pointer (structure, first member must be UserData pointer)

[L7Parser_Context_Destroy] have only one parameters return integer. (void *pContext)
    pContext : Destroy skill Context ponter
Return : (1-Success / 0-Failure)

[L7Parser_Context_Init] have only one parameters return integer. (void *pContext)
    pContext : Destroy skill Context ponter
Return : (1-Success / 0-Failure)

[L7Parser_Parse] have three parameters return integer. (void *pContext, void *pData, size_t nDataSize)
    pContext : Destroy skill Context ponter
    pData: data to be analyzed (continuous partial data)
    nDataSize : data size
Return : (1-Success / 0-Failure)

## Plugins

secrab-http-parser is currently extended with the following plugins.

- [Rocky Linux 9] - open-source enterprise operating system designed to be 
      100% bug-for-bug compatible with Red Hat Enterprise Linux®. 
- [Visual Studio Code] - awesome popular text editor.
- [C/C++] - is a popular programming language.
- [GitLab] - brings teams together to shorten cycle times, 
      reduce costs, strengthen security, and increase developer productivity.

## Installation

Install the dependencies and terminal start the server.

```sh
cd existing repo
git clone https://gitlab.com/erkabyte/secrabs-http-parser/
```
## Development

secrab-http-parser uses llhttp( a standard parsing library for nodejs) and gcc compiler.

Open your favorite Terminal and run these commands.

First Tab:

```sh
 make
```

Second Tab:

```sh
 ./tester_file
```


